

import java.io.*;

/*
Ohjelma ottaa vastaan työpäivien määrän ja sen jälkeen jokaisen
päivän työtunnit.
Tämän jälkeen ohjelma tulostaa tehdyt työtunnit yhteensä, sekä työpäivän
keskimääräisen pituuden.
Tämän lisäksi ohjelma tulostaa syötetyt tuntimäärät.
*/
public class Tunnit {
    public static void main(String[] args) {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        // Taulukko syötetyille tunneille...for silmukka?
        // float tunnit, float keskiarvo, int päivät
        try {
            int paivat = 1;
            double tunnitYhteensa = 0;
            double luvut[] = new double[101];
            int indeksi;
            double syote;
            System.out.println(
                    "Ohjelma laskee yhteen haluamasi ajanjakson aikana tehdyt työtunnit sekä keskimääräisen työpäivän pituuden.\n");
            // Päivien syöttö
            System.out.println("Kuinka monta päivää:");
            paivat = Integer.parseInt(in.readLine());
            for (indeksi = 1; indeksi < paivat + 1; indeksi++) {
                System.out.println("Anna " + indeksi + ". päivän työtunnit:");
                syote = Double.parseDouble(in.readLine());
                luvut[indeksi] = syote;
                tunnitYhteensa = tunnitYhteensa + syote;
                //System.out.println(tunnitYhteensa);

            }
            System.out.println("Tehdyt työtunnit yhteensä: " + tunnitYhteensa);
            System.out.println("Keskimääräinen työpäivän pituus: " + tunnitYhteensa / paivat);
            System.out.print("Syötetyt numerot: ");

            for (indeksi = 1; indeksi < paivat; indeksi++) {
                System.out.print(luvut[indeksi] + " ");
            }

        } catch (Exception e) {
            
            System.out.print("Virhe :/");
        }

    }
}
