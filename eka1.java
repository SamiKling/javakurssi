

import java.io.*;

public class eka1 {
    public static void main(String args[]) {
        String merkkijono = "Anna merkkijono, jonka tulostan: ";
        KysyJaTulosta olio = new KysyJaTulosta();
        olio.teeHommasi(merkkijono);
    }
}

class KysyJaTulosta {
    String syote;
    BufferedReader lukija = new BufferedReader(new InputStreamReader(System.in));

    void teeHommasi(String merkkijono) {
        System.out.println(merkkijono);
        try {
            syote = lukija.readLine();
            System.out.println(syote);
        } catch (IOException e) {
            // virheen kaappaus
            e.printStackTrace();
        }
    }

}
