

import java.io.*;
import java.util.*;

public class Matriisi
{
  public static void main(String[] args)
  {
    int matriisi[][] = new int[5][5];
    int summa;
    String temp;

    try{
      BufferedReader syote = new BufferedReader(new FileReader("./matriisi.txt"));
      for(int x = 0; x < 5; x++){
      temp = syote.readLine();
      StringTokenizer eroitin = new StringTokenizer(temp, "\t");

        for(int y = 0; y < 5; y++){
          matriisi[x][y] = Integer.parseInt(eroitin.nextToken());
        }
      }
      syote.close();

      System.out.print("Matriisi:\n\n");
      tulosta_matriisi(matriisi);
      summa = laske_summa(matriisi);
      System.out.print("\nMatriisin alkioiden summa: " +summa);

    }catch(Exception e){
      System.out.print("Antamasi syöte oli virheellinen..." + e);
    }
  }

  static int laske_summa(int[][] matriisi) {
    int summa = 0;
    for (int[] rivi : matriisi)   // nyt tarvitaan vain alkioiden arvot!
      for (int alkio : rivi)
        summa = summa + alkio;
    return summa;
    
  }

  public static void tulosta_matriisi(int[][] matriisi) {
    for (int rivit=0; rivit<matriisi.length; ++rivit) {
      for (int sarakkeet=0; sarakkeet<matriisi[rivit].length; ++sarakkeet){
        if (sarakkeet <4){
        System.out.print(matriisi[rivit][sarakkeet]+"\t");
        //System.out.println();
        }
        else{
          System.out.print(matriisi[rivit][sarakkeet]);
          System.out.println();
        }
      }
    }
  }


//### put your code here ###
}