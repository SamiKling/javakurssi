

/*
Ohjelma ottaa syötteenä oppilaan koearvosanoja
ja laskee syötettyjen arvosanojen keskiarvon.
Arvosanat voivat olla vain välillä 4-10
Jos käyttäjä antaa negatiivisen luvun, ohjelma laskee keskiarvon.

*/ 

import java.io.*;
public class Keskiarvo
{
public static void main(String[] args)

{
    //Muuttujat arvosanalle, arvosanojen summalle ja arvosanojen lukumäärän summalle.
    //Muuttuja keskiarvolle.

    double arvosana =0;
    int arvosanojenMaara=0;
    double arvosanojenSumma=0;
    double keskiArvo;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
try{
    System.out.println("Ohjelma laskee syotettyjen arvosanojen keskiarvon.");
    System.out.println("Lopetus negatiivisella kokonaisluvulla.");
    //Otetaan kayttajan syote silmukan sisalla.
    while(true){
        System.out.print("Anna arvosana (4-10):");
        arvosana = Integer.parseInt(in.readLine());
        if(arvosana > 3 && arvosana < 11){
            arvosanojenMaara++;
            arvosanojenSumma = arvosanojenSumma+arvosana;
            //System.out.println("Lisattiin Arvosana "+arvosana+". Arvosanojen summa nyt: "+arvosanojenSumma);

        }
        //Jos luku on negatiivinen, tulostetaan keskiarvo ja lopetetaan.
        else if(arvosana < 0){
            keskiArvo = arvosanojenSumma/arvosanojenMaara;
            System.out.println("Ohjelmaan syotetty "+arvosanojenMaara+" arvosanaa.");
            System.out.print("Arvosanojen keskiarvo: "+keskiArvo);
            break;
        }
        //Virheellisen syotteen kasittely.
        else
        {
                System.out.println("Syotetyn arvon tulee olla valilla 4-10.\nJos haluat lopettaa, anna negatiivinen luku.");
        }
    }
            
}
//Muun virheen kasittely.
catch (Exception e)
{
    System.out.println("Tuntematon virhe.");
}
}
}

