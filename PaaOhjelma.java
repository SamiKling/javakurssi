

import java.io.*;

public class PaaOhjelma {
    public static void main(String args[]) {
        Tulostaja olio = new Tulostaja();
        olio.Tulosta();
    }
}

class Tulostaja {
    int tokaLuku;
    int ekaLuku;
    BufferedReader lukija = new BufferedReader(new InputStreamReader(System.in));

    void Tulosta() {
        System.out.println("Syötä ensimmäinen kokonaisluku: ");
        try {
            //Lue käyttäjän syötteet
            ekaLuku = Integer.parseInt(lukija.readLine());
       
            System.out.println("Syötä toinen kokonaisluku: ");
            tokaLuku = Integer.parseInt(lukija.readLine());
            //Laske syötteiden summat käytttäen Laskin metodia. Parametreiksi annetaan käyttäjän syöttämät luvut.
            //Tulosta
            System.out.println("Lukujen summa : "+Laskin.Summa(ekaLuku,tokaLuku));

            } catch (NumberFormatException e) {
            //  Auto-generated catch block
            e.printStackTrace();
        }catch (IOException e) {
            //  Auto-generated catch block
            e.printStackTrace();
}

}
}

//### put your code here ###
class Laskin
{
    static int Summa(int eka, int toka)
    {
        int summa = eka + toka;
        return summa;
    }
}