public class KoiranTesti
{
  public static void main(String[] args)
  {
    Koira rekku = new Koira (2, "Rekku", "Dalmatialainen", "Hau!!!");
    System.out.println("Koiran tiedot:");
    rekku.tulosta_tiedot();
    System.out.println("\nKoira sanoo: " + rekku.annaAani());
  }
} 

class Koira
{
  int ika;
  String nimi, rotu, aani;
  Koira(int ika,String nimi,String rotu,String aani){
    // muodostin
    this.ika = ika;
    this.nimi = nimi;
    this.rotu = rotu;
    this.aani = aani;
    

  }
  public void tulosta_tiedot(){
    // metodi tulosta_tiedot()
    System.out.println(ika);
    System.out.println(nimi);
    System.out.println(rotu);
    

  }
  public String annaAani(){
    // metodi annaAani()
    return aani;
}
}