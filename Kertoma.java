

/*
Laskee käyttäjän antaman syötteen perusteella luvun kertoman.
Esim. Anna kokonaisluku: 9
1*2*3*4*5*6*7*8*9
Luvun 9 kertoma on 362880
*/ 

import java.io.*;
public class Kertoma
{
public static void main(String[] args)

{
    int luku;
    int i=0;
    int kertoma = 1;
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
try{
    System.out.print("Anna kokonaisluku:");
    luku = Integer.parseInt(in.readLine());
    for( i = luku; i > 1; i = i - 1)
    {
    //System.out.println("Luku silmukan alussa: "+i);
    kertoma = kertoma * i;
    //System.out.println("Luku silmukan lopussa: "+i);
    }
    System.out.print("Luvun "+luku+" kertoma on "+kertoma);
}
catch (Exception e)
{
    System.out.println("Syöttämäsi luku on virheellinen.");
}
}
}