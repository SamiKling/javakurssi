import java.io.*;
public class Autoilua
{
    public static void main(String[] args)
    {
        
        BufferedReader lukija = new BufferedReader(new InputStreamReader(System.in));
        int paino, nopeus, km;
        String merkki, malli, rekkari;
        try
        {
            System.out.println("Anna auton merkki:");
            merkki = lukija.readLine();
            System.out.println("Anna auton malli:");
            malli = lukija.readLine();
            System.out.println("Anna auton rekisterinumero:");
            rekkari = lukija.readLine();
            System.out.println("Anna auton paino:");
            paino = Integer.parseInt(lukija.readLine());
            System.out.println("Anna auton huippunopeus:");
            nopeus = Integer.parseInt(lukija.readLine());
            System.out.println("Anna autolla ajetut kilometrit:");
            km = Integer.parseInt(lukija.readLine());
            System.out.println("\n");
            
            Auto autoX = new Auto(paino, nopeus, km, merkki, malli, rekkari, false);
            
            autoX.katsasta();
            autoX.kaynnista();
            autoX.aja(95);
            System.out.print("\n\n");
            
            autoX.katsasta();
        }
        catch (Exception e)
        {
            System.out.println("Virhetilanne!");
        }
    }
} 
class Ajoneuvo
{
   private int paino;
   private int huippunopeus;
   private int ajetut_kilometrit;

   Ajoneuvo(int paino, int huippunopeus, int ajetut_kilometrit)
   {
      this.paino = paino;
      this.huippunopeus = huippunopeus;
      this.ajetut_kilometrit = ajetut_kilometrit;
   }

   protected void aja(int ajettava_matka)
   {
      this.ajetut_kilometrit += ajettava_matka;
   }

   protected int anna_paino()
   {
      return this.paino;
   }

   protected int anna_huippunopeus()
   {
      return this.huippunopeus;
   }

   protected int anna_ajetut_kilometrit()
   {
      return this.ajetut_kilometrit;
   }
} 
class Auto extends Ajoneuvo{
    String merkki;
    String malli;
    String rekisteri_nro;
    boolean kaynnissa;
    int km;
    int tamaPaino;
    int huippunopeus;
    Auto(int paino, int huippunopeus, int ajetut_kilometrit, String merkki, String malli, String rekkari, boolean b) {
        super(paino, huippunopeus, ajetut_kilometrit);
        this.merkki = merkki;
        this.malli = malli;
        this.rekisteri_nro = rekkari;
        this.kaynnissa = b;
        this.km = ajetut_kilometrit;
        this.tamaPaino = paino;
        this.huippunopeus = huippunopeus;
        this.kaynnissa = b;
    }
void katsasta(){
   this.km = super.anna_ajetut_kilometrit();
  System.out.println("Auton tiedot:");
  System.out.println("Merkki: "+merkki);
  System.out.println("Malli: "+malli);
  System.out.println("Ajokilometrit: "+km);
  System.out.println("Paino (kg): "+tamaPaino);
  System.out.println("Huippunopeus (km/h): "+huippunopeus);
  System.out.println("Rekisterinumero: "+rekisteri_nro);
  if (kaynnissa) {
     System.out.println("Auto on käynnissä");
  }
  else{
     System.out.println("Auto ei ole käynnissä");
  }
}
void kaynnista(){
   this.kaynnissa = true;
}

}
/*
Sinun tehtävänä on laatia Ajoneuvo-luokasta periytyvä
luokka "Auto", jolla on seuraavat ominaisuudet:


    String merkki
    String malli
    String rekisteri_nro
    boolean kaynnissa


Lisäksi Auto-luokalla on seuraavat metodit:


    kaynnista(), joka muuttaa kaynnissa-muuttujan arvoksi "true"
    sammuta(), joka muuttaa käynnissä-muuttujan arvoksi "false"
    katsasta(), joka tulostaa auton tiedot esimerkkitulostuksen mukaisesti. */